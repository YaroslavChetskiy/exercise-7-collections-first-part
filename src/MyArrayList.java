import java.util.Arrays;
import java.util.Objects;

public class MyArrayList<E> {
    private int size;
    private Object[] elementData;
    private static final Object[] EMPTY_ELEMENTDATA = {};

    // Конструкторы
    public MyArrayList(int initialCapacity) {
        if (initialCapacity > 0) {
            this.elementData = new Object[initialCapacity];
        } else if(initialCapacity == 0) {
            this.elementData = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
    }

    public MyArrayList() {
        this.elementData = EMPTY_ELEMENTDATA;
    }

    // Функции
    private Object[] grow() {
        if (elementData.length > 0) {
            return elementData = Arrays.copyOf(elementData, size + 1);
        } else {
            return elementData = new Object[1];
        }
    }

    private void checkIndexForAdd(int index) {
        if (index > size || index < 0)
            throw new IndexOutOfBoundsException("Index: "+index+", Size: "+size);
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object o) {
        if (o == null) {
            for (Object el : elementData) {
                if (el == null)
                    return true;
            }
        } else {
            for (Object el : elementData) {
                if (o.equals(el)) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked") // убираем предупреждения
    public E get(int index) {
        Objects.checkIndex(index, size);
        return (E) elementData[index];
    }

    public void clear() {
        for (Object el: elementData) {
            el = null;
        }
        size = 0;
    }

    // Добавление в конец списка
    public boolean add(E e) {
        elementData = grow();
        elementData[size] = e;
        size++;
        return true;
    }

    // Добавление по индексу
    public void add(int index, E element) {
        // Objects.checkIndex(index, size); не подходит из-за того, что size может быть равен index
        checkIndexForAdd(index);
        if (size == index)
            this.add(element);
        else {
            elementData = grow();
            System.arraycopy(elementData, index, elementData, index + 1, size - index);
            size++;
            elementData[index] = element;
        }
    }

    // Удаление элемента (вспомогательная функция, не проверяющая выход индекса за границы и не возвращающая значение)
    private void fastRemove(Object[] eData, int index) {
        size--;
        if (size > index) {
            System.arraycopy(eData, index + 1, eData, index, size - index);
        }
        eData = Arrays.copyOf(eData, size);
    }

    // Удаление по объекту (удаляется первый найденный объект)
    public boolean remove(Object o) {
        int index = 0;
        if (o == null) {
            for (; index < size; index++)
                if (elementData[index] == null)
                    break;
        } else {
            for (; index < size; index++)
                if (o.equals(elementData[index]))
                    break;
        }
        fastRemove(elementData, index);
        return true;
    }
    
    // Удаление по индексу
    @SuppressWarnings("unchecked")
    public E remove(int index) {
        Objects.checkIndex(index, size);
        E oldValue = (E) elementData[index];
        fastRemove(elementData, index);
        return oldValue;
    }
}
