public class Main {
    public static void main(String[] args) {
        /* Функция main не имеет какого-то смысла,
         здесь только небольшие тесты, которые проверяют работоспособность MyArrayList

        MyArrayList<String> arrayList = new MyArrayList<>();
        arrayList.add("Hello");
        arrayList.add("My");
        arrayList.add("Dear");
        arrayList.add("Friend");
        arrayList.add("!");
        System.out.println("-------------- Size: " + arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        arrayList.remove("!");
        System.out.println("-------------- Size: " + arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        arrayList.remove(0);
        System.out.println("-------------- Size: " + arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }

        arrayList.add(0, "Hi");
        arrayList.add(4, "!");
        System.out.println("-------------- Size: " + arrayList.size());
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
        System.out.println("--------------");
        System.out.println(arrayList.contains("HHhhh"));

        System.out.println("--------------");
        System.out.println(arrayList.isEmpty()); // false

        arrayList.clear();
        System.out.println("-------------- Size: " + arrayList.size());
        System.out.println(arrayList.isEmpty()); // true
         */


    }
}